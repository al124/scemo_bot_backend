import uvicorn

from os.path import join, exists

from fastapi import FastAPI

from markov import ScemoBot

app = FastAPI()
BOT = ScemoBot()

@app.get("/save")
async def save(text: str):
    BOT.save_text(text)

@app.get("/read")
async def read():
    text = BOT.get_random_text()
    return {"result":text}

if __name__ == "__main__":
    # eventually read environmental variables from file
    uvicorn.run(app=app)
