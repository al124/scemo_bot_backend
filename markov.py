
import re

from os import makedirs
from os.path import exists, join
import os,random

import numpy as np

import pymongo 

import nltk

nltk.download('punkt')
from nltk.tokenize import word_tokenize

from utils import JSON
SUCCESS, ERROR = 0, 1
PUNTEGGIATURA = {",",";",".","?","(",")","-","\"","$","£","%","@",":","+","*","\/","\\","!","\'"}
class ScemoBot:
    def __init__(self):
        self.start_token, self.end_token = "[start]", "[end]"
    def _read_memory(self):
        return JSON().getJSON("memory.json")
    def _update_memory(self, data: dict = dict()):
        JSON().setJSON("memory.json",data)
        return SUCCESS
    def _random_choice(self,options = dict()):
        lista = list()
        for k in options.keys():
            for i in range(int(options[k])):
                lista.append(k)
        return random.choice(lista)
    def talk(self):
        answer = self.get_random_text()
        return answer
    def save_text(self, text):
        tokens = [self.start_token] + word_tokenize(text) + [self.end_token]
        memory = self._read_memory()
        for i_token, token in enumerate(tokens):
            token = token.strip().lower()
            # the last token is useless
            if i_token == len(tokens) - 1:
                break
            if token not in memory.keys():
                memory[token] = dict() 
            # eventually add tokens to memory
            next_token = tokens[i_token + 1].strip().lower()
            if next_token not in memory[token].keys():
                memory[token][next_token] = 0
            # updates memory
            memory[token][next_token] += 1
        self._update_memory(memory)
    # gets a random text using probabilities from a given chat
    def get_random_text(self):
        memory = self._read_memory()
        if len(memory) == 0:
            return None
        tokens = []
        token_id = self.start_token
        while token_id != self.end_token and len(tokens) < 256:
            token_id = self._random_choice(memory[token_id])
            if token_id not in {self.start_token, self.end_token}:
                tokens.append(token_id)
                if token_id not in memory.keys():
                    break
        result = ""
        for i,e in enumerate(tokens):
            prev = ""
            if i>0:
                prev = tokens[i-1]
            if e in PUNTEGGIATURA:
                result = result + e
            else:
                if prev in PUNTEGGIATURA:
                    if i == 0:
                        result = e
                    else:
                        result = result + " " + e.capitalize()
                else:
                    if i == 0:
                        result = e.capitalize()
                    else:
                        result = result + " " + e
        return result

