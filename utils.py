import json

class JSON:
    def getJSON(self, namefile):
        with open(namefile, "r") as f:
            dictionary = json.load(f)
            return dictionary

    def setJSON(self, namefile, data):
        with open(namefile, "w", encoding="UTF-8") as write_file:
            json.dump(data, write_file, ensure_ascii=False, indent=4, sort_keys=True)

class CSV:
    def getCSV(self, namefile):
        with open(namefile, "r") as f:
            csv = f.read()
            return csv
    def setCSV(self, namefile, data):
        with open(namefile, "w", encoding="UTF-8") as write_file:
            write_file.write(data) 
        
class CSV_TO_JSON:
    def convert(self,in_file,out_file):
        csv = CSV().getCSV(in_file)
        lines = csv.splitlines()
        keys = []
        for i,e in enumerate(lines):
            if e != "":
                k = lines[i].split(",")
                keys.append(k[0])
        attrs = [] 
        for i,e in enumerate(lines[0].split(",")):
            if e != "":
                attrs.append(e)
        result = dict()
        for i,e in enumerate(keys):
            if e != "":
                if i <= 513:
                    result[e] = dict()
                    line = lines[i].split(",")
                    for j,ee in enumerate(line):
                        if  j != 0:
                            if ee != "0.0":
                                if i==105 and j==1:
                                    continue
                                result[e][attrs[j]] = float(ee)
        JSON().setJSON("test.json",result)
        pass
#CSV_TO_JSON().convert("f.csv","out.json")